from django.contrib import admin
from .models import JobCategory, Position, Footer, Blog
from .models import Industry

# Register your models here.
admin.site.register(JobCategory)
admin.site.register(Position)
admin.site.register(Footer)
admin.site.register(Blog)
admin.site.register(Industry)
