# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import colorfield.fields


class Migration(migrations.Migration):

    dependencies = [
        ('cms_custom_plugins', '0002_homefeatureitem_homefeaturelist'),
    ]

    operations = [
        migrations.AddField(
            model_name='homefeaturelist',
            name='background_color',
            field=colorfield.fields.ColorField(default=b'#F9F9F9', max_length=10, verbose_name='Container Background Color'),
        ),
    ]
