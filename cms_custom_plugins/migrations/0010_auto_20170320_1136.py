# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('cms_custom_plugins', '0009_itemoverview'),
    ]

    operations = [
        migrations.AlterField(
            model_name='parallaxcontainer',
            name='min_height',
            field=models.CharField(default=b'200px', max_length=100, verbose_name='Minimun height'),
        ),
    ]
