# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('cms_custom_plugins', '0010_auto_20170320_1136'),
        ('cms_custom_plugins', '0010_auto_20170319_1817'),
    ]

    operations = [
    ]
