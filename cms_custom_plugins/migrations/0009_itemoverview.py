# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import colorfield.fields


class Migration(migrations.Migration):

    dependencies = [
        ('cms', '0016_auto_20160608_1535'),
        ('cms_custom_plugins', '0008_parallaxcontainer_padding_top'),
    ]

    operations = [
        migrations.CreateModel(
            name='ItemOverview',
            fields=[
                ('cmsplugin_ptr', models.OneToOneField(parent_link=True, related_name='cms_custom_plugins_itemoverview', auto_created=True, primary_key=True, serialize=False, to='cms.CMSPlugin')),
                ('title', models.CharField(max_length=200, verbose_name='Item Title')),
                ('title_color', colorfield.fields.ColorField(default=b'#0695ab', max_length=10, verbose_name='Item Title Color')),
                ('overview', models.TextField(verbose_name='Item Overview Text')),
            ],
            options={
                'abstract': False,
            },
            bases=('cms.cmsplugin',),
        ),
    ]
