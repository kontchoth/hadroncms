# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import colorfield.fields


class Migration(migrations.Migration):

    dependencies = [
        ('cms', '0016_auto_20160608_1535'),
        ('cms_custom_plugins', '0004_parallaxcontainer'),
    ]

    operations = [
        migrations.CreateModel(
            name='HadronButton',
            fields=[
                ('cmsplugin_ptr', models.OneToOneField(parent_link=True, related_name='cms_custom_plugins_hadronbutton', auto_created=True, primary_key=True, serialize=False, to='cms.CMSPlugin')),
                ('button_label', models.CharField(default=b'Button', max_length=100, verbose_name='Button Label')),
                ('label_color', colorfield.fields.ColorField(default=b'#ffffff', max_length=10, verbose_name='Button Label Color')),
                ('button_border_color', colorfield.fields.ColorField(default=b'#0695ab', max_length=10, verbose_name='Button Border Color')),
                ('button_position', models.CharField(default=b'left', max_length=100, verbose_name='Button Position', choices=[(b'left', b'left'), (b'right', b'right'), (b'center', b'center')])),
                ('button_url', models.CharField(default=b'#', max_length=200, verbose_name='Button url')),
            ],
            options={
                'abstract': False,
            },
            bases=('cms.cmsplugin',),
        ),
        migrations.CreateModel(
            name='HadronButtonGroup',
            fields=[
                ('cmsplugin_ptr', models.OneToOneField(parent_link=True, related_name='cms_custom_plugins_hadronbuttongroup', auto_created=True, primary_key=True, serialize=False, to='cms.CMSPlugin')),
                ('height', models.IntegerField(default=50, verbose_name='Button Group height')),
                ('width', models.CharField(default=b'80%', max_length=100, verbose_name='Button Group max width (% or px')),
            ],
            options={
                'abstract': False,
            },
            bases=('cms.cmsplugin',),
        ),
    ]
