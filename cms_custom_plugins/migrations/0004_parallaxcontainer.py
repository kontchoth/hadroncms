# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import colorfield.fields


class Migration(migrations.Migration):

    dependencies = [
        ('cms', '0016_auto_20160608_1535'),
        ('cms_custom_plugins', '0003_homefeaturelist_background_color'),
    ]

    operations = [
        migrations.CreateModel(
            name='ParallaxContainer',
            fields=[
                ('cmsplugin_ptr', models.OneToOneField(parent_link=True, related_name='cms_custom_plugins_parallaxcontainer', auto_created=True, primary_key=True, serialize=False, to='cms.CMSPlugin')),
                ('background_img', models.ImageField(upload_to=b'parallax/bg/', verbose_name='Background Image')),
                ('min_height', models.IntegerField(default=200, verbose_name='Minimun height')),
                ('title', models.CharField(max_length=100, verbose_name='Container Title')),
                ('title_color', colorfield.fields.ColorField(default=b'#FFF', max_length=10, verbose_name='Title Color')),
                ('slogan', models.CharField(max_length=100, verbose_name='Container Slogan')),
                ('slogan_color', colorfield.fields.ColorField(default=b'#0695ab', max_length=10, verbose_name='Slogan Color')),
            ],
            options={
                'abstract': False,
            },
            bases=('cms.cmsplugin',),
        ),
    ]
