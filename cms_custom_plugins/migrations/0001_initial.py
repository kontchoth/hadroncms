# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import colorfield.fields
import djangocms_text_ckeditor.fields
import cms_custom_plugins.validators


class Migration(migrations.Migration):

    dependencies = [
        ('cms', '0016_auto_20160608_1535'),
    ]

    operations = [
        migrations.CreateModel(
            name='Banner',
            fields=[
                ('cmsplugin_ptr', models.OneToOneField(parent_link=True, related_name='cms_custom_plugins_banner', auto_created=True, primary_key=True, serialize=False, to='cms.CMSPlugin')),
                ('title', models.CharField(max_length=100)),
                ('image', models.ImageField(null=True, upload_to=b'banners/', blank=True)),
                ('video', models.FileField(blank=True, null=True, upload_to=b'videos/', validators=[cms_custom_plugins.validators.validate_video_extension])),
                ('video_bg', models.BooleanField(default=False)),
            ],
            options={
                'abstract': False,
            },
            bases=('cms.cmsplugin',),
        ),
        migrations.CreateModel(
            name='BannerDisplay',
            fields=[
                ('cmsplugin_ptr', models.OneToOneField(parent_link=True, related_name='cms_custom_plugins_bannerdisplay', auto_created=True, primary_key=True, serialize=False, to='cms.CMSPlugin')),
                ('background_color', colorfield.fields.ColorField(default=b'#0695ab', max_length=10, verbose_name='Banner background color')),
                ('height', models.IntegerField(default=-1, verbose_name='Heigh of the banner display.(default to auto')),
                ('has_arrow', models.BooleanField(default=False, verbose_name='Should show arrow')),
                ('arrow_direction', models.CharField(blank=True, max_length=100, null=True, verbose_name='Arrow direction', choices=[(b'up', b'up'), (b'down', b'down'), (b'left', b'left'), (b'right', b'right')])),
                ('has_text', models.BooleanField(default=False, verbose_name='Does banner has text')),
                ('show_title', models.BooleanField(default=False, verbose_name='Display Title')),
                ('title', models.CharField(max_length=200, null=True, verbose_name='Title', blank=True)),
                ('title_position', models.CharField(default=b'left', max_length=50, verbose_name='Title Position', choices=[(b'left', b'left'), (b'right', b'right'), (b'center', b'center')])),
                ('title_color', colorfield.fields.ColorField(default=b'#0695ab', max_length=10, verbose_name='Title Color')),
                ('show_button', models.BooleanField(default=False, verbose_name='Display Button ?')),
                ('button_url', models.CharField(max_length=200, null=True, verbose_name='Button url', blank=True)),
                ('button_label', models.CharField(max_length=100, null=True, verbose_name='Button Label', blank=True)),
                ('button_label_color', colorfield.fields.ColorField(default=b'#000000', max_length=10, verbose_name='Button Label Color')),
                ('button_bg_color', colorfield.fields.ColorField(default=b'#0695ab', max_length=10, verbose_name='Button Background color')),
                ('button_bg_hover_color', colorfield.fields.ColorField(default=b'#0695ab', max_length=10, verbose_name='Button background color on hover')),
                ('button_position', models.CharField(default=b'left', max_length=50, verbose_name='Button position', choices=[(b'left', b'left'), (b'right', b'right'), (b'center', b'center')])),
                ('margin_bottom', models.IntegerField(default=0, verbose_name='Margin bottom')),
                ('text', djangocms_text_ckeditor.fields.HTMLField(blank=True)),
            ],
            options={
                'abstract': False,
            },
            bases=('cms.cmsplugin',),
        ),
        migrations.CreateModel(
            name='Blog',
            fields=[
                ('cmsplugin_ptr', models.OneToOneField(parent_link=True, related_name='cms_custom_plugins_blog', auto_created=True, primary_key=True, serialize=False, to='cms.CMSPlugin')),
                ('display_img', models.ImageField(upload_to=b'blogs/images/')),
                ('title', models.CharField(max_length=200)),
                ('author', models.CharField(max_length=200)),
                ('posted_date', models.DateTimeField(auto_now_add=True)),
                ('has_social', models.BooleanField(default=True)),
                ('content', djangocms_text_ckeditor.fields.HTMLField()),
            ],
            options={
                'abstract': False,
            },
            bases=('cms.cmsplugin',),
        ),
        migrations.CreateModel(
            name='Footer',
            fields=[
                ('cmsplugin_ptr', models.OneToOneField(parent_link=True, related_name='cms_custom_plugins_footer', auto_created=True, primary_key=True, serialize=False, to='cms.CMSPlugin')),
                ('transparent_bg', models.BooleanField(default=False)),
                ('background_img', models.ImageField(default=b'/static/images/footer/footer-bg.jpg', upload_to=b'bg/footer/')),
                ('logo_img', models.ImageField(default=b'/static/images/footer/logo-top.png', upload_to=b'bg/footer/')),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('updated_at', models.DateTimeField(auto_now=True)),
            ],
            options={
                'abstract': False,
            },
            bases=('cms.cmsplugin',),
        ),
        migrations.CreateModel(
            name='Industry',
            fields=[
                ('cmsplugin_ptr', models.OneToOneField(parent_link=True, related_name='cms_custom_plugins_industry', auto_created=True, primary_key=True, serialize=False, to='cms.CMSPlugin')),
                ('name', models.CharField(max_length=200, verbose_name='Industry name')),
                ('background_img', models.ImageField(upload_to=b'industries/', verbose_name='Industry display image')),
            ],
            options={
                'abstract': False,
            },
            bases=('cms.cmsplugin',),
        ),
        migrations.CreateModel(
            name='JobCategory',
            fields=[
                ('cmsplugin_ptr', models.OneToOneField(parent_link=True, related_name='cms_custom_plugins_jobcategory', auto_created=True, primary_key=True, serialize=False, to='cms.CMSPlugin')),
                ('name', models.CharField(max_length=200)),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('updated_at', models.DateTimeField(auto_now=True)),
            ],
            options={
                'abstract': False,
            },
            bases=('cms.cmsplugin',),
        ),
        migrations.CreateModel(
            name='JobCategoryList',
            fields=[
                ('cmsplugin_ptr', models.OneToOneField(parent_link=True, related_name='cms_custom_plugins_jobcategorylist', auto_created=True, primary_key=True, serialize=False, to='cms.CMSPlugin')),
            ],
            options={
                'abstract': False,
            },
            bases=('cms.cmsplugin',),
        ),
        migrations.CreateModel(
            name='Position',
            fields=[
                ('cmsplugin_ptr', models.OneToOneField(parent_link=True, related_name='cms_custom_plugins_position', auto_created=True, primary_key=True, serialize=False, to='cms.CMSPlugin')),
                ('title', models.CharField(max_length=200)),
                ('slug', models.CharField(unique=True, max_length=200)),
                ('is_active', models.BooleanField(default=False)),
                ('description', djangocms_text_ckeditor.fields.HTMLField(blank=True)),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('updated_at', models.DateTimeField(auto_now=True)),
                ('category', models.ForeignKey(related_name='category', to='cms_custom_plugins.JobCategory')),
            ],
            options={
                'abstract': False,
            },
            bases=('cms.cmsplugin',),
        ),
        migrations.CreateModel(
            name='Service',
            fields=[
                ('cmsplugin_ptr', models.OneToOneField(parent_link=True, related_name='cms_custom_plugins_service', auto_created=True, primary_key=True, serialize=False, to='cms.CMSPlugin')),
                ('name', models.CharField(max_length=200, verbose_name='Service name')),
                ('overview', models.TextField(verbose_name='Overview Description')),
                ('has_icon', models.BooleanField(default=False, verbose_name='Service has icon?')),
                ('icon_position', models.CharField(default=b'left', max_length=50, verbose_name='Icon position', choices=[(b'up', b'up'), (b'down', b'down'), (b'left', b'left'), (b'right', b'right')])),
                ('icon_position_sm', models.CharField(default=b'top', max_length=50, verbose_name='Icon position on small', choices=[(b'up', b'up'), (b'down', b'down'), (b'left', b'left'), (b'right', b'right')])),
                ('icon', models.ImageField(upload_to=b'services/icons/', null=True, verbose_name='Service Icon', blank=True)),
                ('show_button', models.BooleanField(default=False, verbose_name='Display Button ?')),
                ('button_url', models.CharField(max_length=200, null=True, verbose_name='Button url', blank=True)),
                ('button_label', models.CharField(max_length=100, null=True, verbose_name='Button Label', blank=True)),
                ('button_label_color', colorfield.fields.ColorField(default=b'#000000', max_length=10, verbose_name='Button Label Color')),
                ('button_bg_color', colorfield.fields.ColorField(default=b'#0695ab', max_length=10, verbose_name='Button Background color')),
                ('button_bg_hover_color', colorfield.fields.ColorField(default=b'#0695ab', max_length=10, verbose_name='Button background color on hover')),
                ('button_position', models.CharField(default=b'right', max_length=50, verbose_name='Button position', choices=[(b'left', b'left'), (b'right', b'right'), (b'center', b'center')])),
            ],
            options={
                'abstract': False,
            },
            bases=('cms.cmsplugin',),
        ),
        migrations.CreateModel(
            name='Slider',
            fields=[
                ('cmsplugin_ptr', models.OneToOneField(parent_link=True, related_name='cms_custom_plugins_slider', auto_created=True, primary_key=True, serialize=False, to='cms.CMSPlugin')),
                ('slide_duration', models.IntegerField(default=3000, verbose_name='Slide duration (ms)')),
            ],
            options={
                'abstract': False,
            },
            bases=('cms.cmsplugin',),
        ),
        migrations.CreateModel(
            name='SliderItem',
            fields=[
                ('cmsplugin_ptr', models.OneToOneField(parent_link=True, related_name='cms_custom_plugins_slideritem', auto_created=True, primary_key=True, serialize=False, to='cms.CMSPlugin')),
                ('image', models.ImageField(upload_to=b'slider/images/')),
                ('title', models.CharField(max_length=200, null=True, blank=True)),
                ('message', models.CharField(max_length=200, null=True, blank=True)),
            ],
            options={
                'abstract': False,
            },
            bases=('cms.cmsplugin',),
        ),
    ]
