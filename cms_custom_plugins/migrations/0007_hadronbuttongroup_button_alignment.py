# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('cms_custom_plugins', '0006_homeindustrycontainer'),
    ]

    operations = [
        migrations.AddField(
            model_name='hadronbuttongroup',
            name='button_alignment',
            field=models.CharField(default=None, max_length=100, null=True, verbose_name='Button Alignment', choices=[(b'left', b'left'), (b'right', b'right'), (b'center', b'center'), (None, None)]),
        ),
    ]
