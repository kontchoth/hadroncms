# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import colorfield.fields


class Migration(migrations.Migration):

    dependencies = [
        ('cms', '0016_auto_20160608_1535'),
        ('cms_custom_plugins', '0005_hadronbutton_hadronbuttongroup'),
    ]

    operations = [
        migrations.CreateModel(
            name='HomeIndustryContainer',
            fields=[
                ('cmsplugin_ptr', models.OneToOneField(parent_link=True, related_name='cms_custom_plugins_homeindustrycontainer', auto_created=True, primary_key=True, serialize=False, to='cms.CMSPlugin')),
                ('title', models.CharField(max_length=100, verbose_name='Container Title')),
                ('title_color', colorfield.fields.ColorField(default=b'#FFF', max_length=10, verbose_name='Title Color')),
                ('slogan', models.CharField(max_length=100, verbose_name='Container Slogan')),
                ('slogan_color', colorfield.fields.ColorField(default=b'#0695ab', max_length=10, verbose_name='Slogan Color')),
            ],
            options={
                'abstract': False,
            },
            bases=('cms.cmsplugin',),
        ),
    ]
