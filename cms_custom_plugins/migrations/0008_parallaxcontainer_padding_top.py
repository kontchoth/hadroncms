# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('cms_custom_plugins', '0007_hadronbuttongroup_button_alignment'),
    ]

    operations = [
        migrations.AddField(
            model_name='parallaxcontainer',
            name='padding_top',
            field=models.IntegerField(default=20, verbose_name='Padding top'),
        ),
    ]
