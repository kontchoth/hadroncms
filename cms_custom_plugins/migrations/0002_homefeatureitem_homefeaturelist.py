# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import colorfield.fields
import djangocms_text_ckeditor.fields


class Migration(migrations.Migration):

    dependencies = [
        ('cms', '0016_auto_20160608_1535'),
        ('cms_custom_plugins', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='HomeFeatureItem',
            fields=[
                ('cmsplugin_ptr', models.OneToOneField(parent_link=True, related_name='cms_custom_plugins_homefeatureitem', auto_created=True, primary_key=True, serialize=False, to='cms.CMSPlugin')),
                ('icon', models.ImageField(upload_to=b'features/icons/')),
                ('description', djangocms_text_ckeditor.fields.HTMLField()),
            ],
            options={
                'abstract': False,
            },
            bases=('cms.cmsplugin',),
        ),
        migrations.CreateModel(
            name='HomeFeatureList',
            fields=[
                ('cmsplugin_ptr', models.OneToOneField(parent_link=True, related_name='cms_custom_plugins_homefeaturelist', auto_created=True, primary_key=True, serialize=False, to='cms.CMSPlugin')),
                ('title', models.CharField(max_length=200, null=True, verbose_name='Container Title', blank=True)),
                ('title_color', colorfield.fields.ColorField(default=b'#333', max_length=10, verbose_name='Title Color')),
                ('slogan', models.CharField(max_length=200, null=True, verbose_name='Container Slogan', blank=True)),
                ('slogan_color', colorfield.fields.ColorField(default=b'#0695ab', max_length=10, verbose_name='Slogan Color')),
                ('has_line', models.BooleanField(default=False)),
                ('line_color', colorfield.fields.ColorField(default=b'#444', max_length=10, verbose_name='Line Color')),
            ],
            options={
                'abstract': False,
            },
            bases=('cms.cmsplugin',),
        ),
    ]
