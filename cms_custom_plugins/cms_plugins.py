from cms.plugin_base import CMSPluginBase
from django.contrib import admin
from cms.plugin_pool import plugin_pool
from cms.models.pluginmodel import CMSPlugin
from django.utils.translation import ugettext_lazy as _
from forms_builder.forms.models import Form
from .models import Banner, JobCategory, JobCategoryList, Position
from .models import Footer, Blog, BannerDisplay, Industry, Service
from .models import Slider, SliderItem, HomeFeatureList, HomeFeatureItem
from .models import ParallaxContainer, HadronButtonGroup, HadronButton
from .models import HomeIndustryContainer, ItemOverview
import requests, json

GOOGLE_RES_API_KEY = 'AIzaSyAL6mR5zncBcmXmY-eNa9E-RIXopy1co4g'
GOOGLE_CX_KEY = '006697635909584198551:hpryxbtjdfk'
class BannerPlugin(CMSPluginBase):
	model = Banner
	name = _('Page Banner')
	render_template = 'plugins/banners/banner.template.html'
	cache = False

	def render(self, context, instance, placeholder):
		context = super(BannerPlugin, self).render(context, instance, placeholder)
		return context
plugin_pool.register_plugin(BannerPlugin)

class JobCategoryListPlugin(CMSPluginBase):
	model = JobCategoryList
	name = _('Job Category List')
	render_template = 'plugins/jobs/job.categories.template.html'
	cache = False
	allow_children = True
	child_classes = ['PositionPlugin']

	def render(self, context, instance, placeholder):
		categories = JobCategory.objects.all()
		positions = []
		for category in categories:
			positions.append({
				'category': category,
				'positions': Position.objects.all().filter(category=category, is_active=True)
			})
		print positions
		context.update({'categories': categories, 'positions': positions})
		context = super(JobCategoryListPlugin, self).render(context, instance, placeholder)
		return context
plugin_pool.register_plugin(JobCategoryListPlugin)

class PositionPlugin(CMSPluginBase):
	model = Position
	name = _('Position')
	render_template = 'plugins/jobs/job.category.template.html'
	cache = False
	require_parent = True
	parent_classes = ['JobCategoryListPlugin']

	def render(self, context, instance, placeholder):
		context = super(PositionPlugin, self).render(context, instance, placeholder)
		return context
plugin_pool.register_plugin(PositionPlugin)

class FooterPlugin(CMSPluginBase):
	model = Footer
	name = _('Footer')
	render_template = 'plugins/footers/footer.html'
	cache = False

	def render(self, context, instance, placeholder):
		context = super(FooterPlugin, self).render(context, instance, placeholder)
		return context
plugin_pool.register_plugin(FooterPlugin)

class JobDescriptionPlugin(CMSPluginBase):
	name = _('Position Details')
	render_template = 'plugins/jobs/job.details.html'
	cache = False

	def render(self, context, instance, placeholder):
		request = context['request']
		position_id = request.GET.get('id', None)
		categories = JobCategory.objects.all()
		# form = Form.objects.get(title='Job Application')
		form = None
		position = None
		if position_id:
			position = Position.objects.get(id=position_id)
		context.update({'position': position, 'categories': categories, 'form':form})
		context = super(JobDescriptionPlugin, self).render(context, instance, placeholder)
		return context
plugin_pool.register_plugin(JobDescriptionPlugin)

class BlogOverviewPlugin(CMSPluginBase):
	name = _('Blogs Overview')
	cache = False
	render_template = 'plugins/blogs/blogs.overview.html'

	def render(self, context, instance, placeholder):
		blogs = Blog.objects.all().order_by('-posted_date')[:3]
		context.update({'blogs': blogs})
		context = super(BlogOverviewPlugin, self).render(context, instance, placeholder)
		return context
plugin_pool.register_plugin(BlogOverviewPlugin)

class BannerDisplayPlugin(CMSPluginBase):
	model = BannerDisplay
	name = _('Banner Display')
	render_template = 'plugins/banners/banner.display.html'
	allow_children = True
	child_classes = ['HadronButtonGroupPlugin']

	def render(self, context, instance, placeholder):
		context = super(BannerDisplayPlugin, self).render(context, instance, placeholder)
		return context
plugin_pool.register_plugin(BannerDisplayPlugin)

class IndustryListPlugin(CMSPluginBase):
	name = _('Industries List')
	model = HomeIndustryContainer
	allow_children = True
	child_classes = ['IndustryPlugin']
	render_template = 'plugins/industries/industry.list.html'

	def render(self, context, instance, placeholder):
		context = super(IndustryListPlugin, self).render(context, instance, placeholder)
		return context
plugin_pool.register_plugin(IndustryListPlugin)

class IndustryPlugin(CMSPluginBase):
	model = Industry
	name = _('Industry Item')
	render_template = 'plugins/industries/industry.html'
	require_parent = True
	parent_classes = ['IndustryListPlugin']

	def render(self, context, instance, placeholder):
		context = super(IndustryPlugin, self).render(context, instance, placeholder)
		return context
plugin_pool.register_plugin(IndustryPlugin)

class ServiceListPlugin(CMSPluginBase):
	name = _('Service List Container')
	allow_children = True
	child_classes = ['ServicePlugin']
	render_template = 'plugins/services/service.list.html'

	def render(self, context, instance, placeholder):
		context = super(ServiceListPlugin, self).render(context, instance, placeholder)
		return context
plugin_pool.register_plugin(ServiceListPlugin)

class ServicePlugin(CMSPluginBase):
	model = Service
	name = _('Service Item')
	render_template = 'plugins/services/service.html'
	require_parent = True
	parent_classes = ['ServiceListPlugin']

	def render(self, context, instance, placeholder):
		context = super(ServicePlugin, self).render(context, instance, placeholder)
		return context
plugin_pool.register_plugin(ServicePlugin)

class SliderPlugin(CMSPluginBase):
	name = _('Slider Container')
	allow_children = True
	model = Slider
	render_template = 'plugins/slider/slider.html'
	cache = False
	child_classes = ['SliderItemPlugin']

	def render(self, context, instance, placeholder):
		context = super(SliderPlugin, self).render(context, instance, placeholder)
		return context
plugin_pool.register_plugin(SliderPlugin)

class SliderItemPlugin(CMSPluginBase):
	model = SliderItem
	name = _('Slider Item')
	require_parent = True
	cache = False
	parent_classes = ['SliderPlugin']
	render_template = 'plugins/slider/slide.item.html'

	def render(self, context, instance, placeholder):
		context = super(SliderItemPlugin, self).render(context, instance, placeholder)
		return context
plugin_pool.register_plugin(SliderItemPlugin)

class HomeFeatureContainerPlugin(CMSPluginBase):
	name = _('Home Features Container')
	allow_children = True
	model = HomeFeatureList
	render_template = 'plugins/features/feature.container.html'
	cache = False
	child_classes = ['HomeFeatureItemPlugin', 'ItemOverviewPlugin']

	def render(self, context, instance, placeholder):
		context = super(HomeFeatureContainerPlugin, self).render(context, instance, placeholder)
		return context
plugin_pool.register_plugin(HomeFeatureContainerPlugin)

class HomeFeatureItemPlugin(CMSPluginBase):
	model = HomeFeatureItem
	name = _('Home Feature Item')
	require_parent = True
	cache = False
	parent_classes = ['HomeFeatureContainerPlugin']
	render_template = 'plugins/features/feature.item.html'

	def render(self, context, instance, placeholder):
		context = super(HomeFeatureItemPlugin, self).render(context, instance, placeholder)
		return context
plugin_pool.register_plugin(HomeFeatureItemPlugin)

class ParallaxContainerPlugin(CMSPluginBase):
	model = ParallaxContainer
	name = _('Parallax Container')
	cache = False
	allow_children = True
	render_template = 'plugins/parallax/parallax.container.html'
	child_classes = []

	def render(self, context, instance, placeholder):
		context = super(ParallaxContainerPlugin, self).render(context, instance, placeholder)
		return context	
plugin_pool.register_plugin(ParallaxContainerPlugin)

class HadronButtonGroupPlugin(CMSPluginBase):
	model = HadronButtonGroup
	name = _('Hadron Button Group')
	cache = False
	allow_children = True
	child_classes = ['HadronButtonPlugin']
	render_template = 'plugins/buttons/button.group.html'

	def render(self, context, instance, placeholder):
		context = super(HadronButtonGroupPlugin, self).render(context, instance, placeholder)
		return context
plugin_pool.register_plugin(HadronButtonGroupPlugin)

class HadronButtonPlugin(CMSPluginBase):
	model = HadronButton
	name = _('Hadron Button')
	cache = False
	require_parent = True
	parent_classes = ['HadronButtonGroupPlugin']
	render_template = 'plugins/buttons/button.html'

	def render(self, context, instance, placeholder):
		context = super(HadronButtonPlugin, self).render(context, instance, placeholder)
		return context
plugin_pool.register_plugin(HadronButtonPlugin)

class ItemOverviewPlugin(CMSPluginBase):
	model = ItemOverview
	name = _('Item Overview')
	cache = False
	render_template = 'plugins/items/item.overview.html'

	def render(self, context, instance, placeholder):
		context = super(ItemOverviewPlugin, self).render(context, instance, placeholder)
		return context
plugin_pool.register_plugin(ItemOverviewPlugin)

class SearchEngineResultPlugin(CMSPluginBase):
	name = _('Search Engine Result')
	cache = False
	render_template = 'plugins/searches/search.results.html'

	def render(self, context, instance, placeholder):
		request = context['request']
		search_params = request.GET.get('q', '')
		url = 'https://www.googleapis.com/customsearch/v1?key='
		url += GOOGLE_RES_API_KEY + '&cx='
		url += GOOGLE_CX_KEY + '&q='
		url += search_params
		response = requests.get(url)
		results = {
			'items': [],
			'search_params': search_params,
			'default': 'No Results'
		}
		if response.status_code == 200:
			resp = json.loads(response.text)
			if 'items' in resp.keys():
				results['items'] = resp['items']
		context.update({'results': results})
		context = super(SearchEngineResultPlugin, self).render(context, instance, placeholder);
		return context
plugin_pool.register_plugin(SearchEngineResultPlugin)