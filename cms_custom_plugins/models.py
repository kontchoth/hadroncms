from cms.models.pluginmodel import CMSPlugin
from django.utils.translation import ugettext_lazy as _
from .validators import validate_video_extension
from djangocms_text_ckeditor.fields import HTMLField
from colorfield.fields import ColorField

from django.db import models

ARROW_CHOICES = (('up', 'up'),('down', 'down'),('left', 'left'),('right', 'right'))
TITLE_POSITIONS = (('left', 'left'),('right', 'right'), ('center', 'center'))
ALIGMENTS = (('left', 'left'),('right', 'right'), ('center', 'center'), (None, None))

class Banner(CMSPlugin):
	title = models.CharField(max_length=100, blank=False, null=False)
	image = models.ImageField(upload_to = 'banners/', null=True, blank=True)
	video = models.FileField(upload_to="videos/", validators=[validate_video_extension],null=True, blank=True)
	video_bg = models.BooleanField(default=False)

class JobCategory(CMSPlugin):
	name = models.CharField(max_length=200, blank=False, null=False)
	created_at = models.DateTimeField(auto_now_add=True)
	updated_at = models.DateTimeField(auto_now=True)

	def __str__(self):
		return self.name

	def __unicode__(self):
		return self.name

class JobCategoryList(CMSPlugin):
	# no field

	def __str__(self):
		return 'Job Category List'

class Position(CMSPlugin):
	category = models.ForeignKey(JobCategory, related_name='category')
	title = models.CharField(max_length=200, blank=False, null=False)
	slug = models.CharField(max_length=200, blank=False, null=False, unique=True)
	is_active = models.BooleanField(default=False)
	description = HTMLField(blank=True)
	created_at = models.DateTimeField(auto_now_add=True)
	updated_at = models.DateTimeField(auto_now=True)

	def __str__(self):
		return '{} - {}'.format(self.category, self.title)

	def __unicode__(self):
		return '{} - {}'.format(self.category, self.title)

class Footer(CMSPlugin):
	transparent_bg = models.BooleanField(default=False)
	background_img = models.ImageField(upload_to='bg/footer/', blank=False, default='/static/images/footer/footer-bg.jpg')
	logo_img = models.ImageField(upload_to='bg/footer/', blank=False, default='/static/images/footer/logo-top.png')
	
	created_at = models.DateTimeField(auto_now_add=True)
	updated_at = models.DateTimeField(auto_now=True)


	def __str__(self):
		return 'Footer'

	def __unicode__(self):
		return 'Footer'

class Blog(CMSPlugin):
	display_img = models.ImageField(upload_to='blogs/images/', blank=False, null=False)
	title = models.CharField(max_length=200, null=False, blank=False)
	author = models.CharField(max_length=200, null=False, blank=False)
	posted_date = models.DateTimeField(auto_now_add=True)
	has_social = models.BooleanField(default=True)
	content = HTMLField(null=False, blank=False)

	def __str__(self):
		return self.title

	def __unicode__(self):
		return self.title

class BannerDisplay(CMSPlugin):
	background_color = ColorField(_('Banner background color'),default='#0695ab', null=False, blank=False)
	height = models.IntegerField(_('Heigh of the banner display.(default to auto'),default=-1, null=False, blank=False)

	# Arrow details
	has_arrow = models.BooleanField(_('Should show arrow'),default=False)
	arrow_direction = models.CharField(_('Arrow direction'),choices=ARROW_CHOICES, null=True, blank=True, max_length=100)
	
		
	has_text = models.BooleanField(_('Does banner has text'),default=False)
	
	# title details
	show_title = models.BooleanField(_('Display Title'), default=False)
	title = models.CharField(_('Title'), max_length=200, blank=True, null=True)
	title_position = models.CharField(_('Title Position'),max_length=50, choices=TITLE_POSITIONS, null=False, blank=False, default='left')
	title_color = ColorField(_('Title Color'),default='#0695ab', null=False, blank=False)

	# button details
	show_button = models.BooleanField(_('Display Button ?'),default=False)
	button_url = models.CharField(_('Button url'),max_length=200, null=True, blank=True)
	button_label = models.CharField(_('Button Label'),max_length=100, null=True, blank=True)
	button_label_color = ColorField(_('Button Label Color'),default='#000000', null=False, blank=False)
	button_bg_color = ColorField(_('Button Background color'),default='#0695ab', null=False, blank=False)
	button_bg_hover_color = ColorField(_('Button background color on hover'),default='#0695ab', null=False, blank=False)
	button_position = models.CharField(_('Button position'),max_length=50, choices=TITLE_POSITIONS, null=False, blank=False, default='left')

	margin_bottom = models.IntegerField(_('Margin bottom'), null=False, blank=False, default=0)
	text = HTMLField(blank=True)
	
	def __unicode__(self):
		return 'Banner Display'

	def __str__(self):
		return 'Banner Display'

class Industry(CMSPlugin):
	name = models.CharField(_('Industry name'), max_length=200, null=False, blank=False)
	background_img = models.ImageField(_('Industry display image'), upload_to='industries/', null=False, blank=False)

	def __inicode__(self):
		return 'Industry - {}'.format(self.name)

	def __str__(self):
		return 'Industry - {}'.format(self.name)

class Service(CMSPlugin):
	name = models.CharField(_('Service name'), max_length=200, blank=False, null=False)
	overview = models.TextField(_('Overview Description'), blank=False)
	
	# Service icon information
	has_icon = models.BooleanField(_('Service has icon?'), default=False)
	icon_position = models.CharField(_('Icon position'),max_length=50, choices=ARROW_CHOICES, null=False, blank=False, default='left')
	icon_position_sm = models.CharField(_('Icon position on small'),max_length=50, choices=ARROW_CHOICES, null=False, blank=False, default='top')
	icon = models.ImageField(_('Service Icon'), upload_to='services/icons/', blank=True, null=True)


	# button information
	show_button = models.BooleanField(_('Display Button ?'),default=False)
	button_url = models.CharField(_('Button url'),max_length=200, null=True, blank=True)
	button_label = models.CharField(_('Button Label'),max_length=100, null=True, blank=True)
	button_label_color = ColorField(_('Button Label Color'),default='#000000', null=False, blank=False)
	button_bg_color = ColorField(_('Button Background color'),default='#0695ab', null=False, blank=False)
	button_bg_hover_color = ColorField(_('Button background color on hover'),default='#0695ab', null=False, blank=False)
	button_position = models.CharField(_('Button position'),max_length=50, choices=TITLE_POSITIONS, null=False, blank=False, default='right')

	def __str__(self):
		return 'Single Service'

	def __unicode__(self):
		return 'Single Service'

class Slider(CMSPlugin):
	slide_duration = models.IntegerField(_('Slide duration (ms)'), null=False, blank=False, default=3000)

	def __unicode__(self):
		return 'Slider {}ms'.format(self.slide_duration)

	def __str__(self):
		return 'Slider {}ms'.format(self.slide_duration)

class SliderItem(CMSPlugin):
	image = models.ImageField(upload_to='slider/images/', blank=False, null=False)
	title = models.CharField(null=True, blank=True, max_length=200)
	message = models.CharField(null=True, blank=True, max_length=200)


	def __inicode__(self):
		return 'Slide Item {}'.format(self.title)

	def __str__(self):
		return 'Slide Item {}'.format(self.title)

class HomeFeatureList(CMSPlugin):
	background_color = ColorField(_('Container Background Color'), default='#F9F9F9')
	title = models.CharField(_('Container Title'), max_length=200, null=True, blank=True)
	title_color = ColorField(_('Title Color'), default='#333')
	slogan = models.CharField(_('Container Slogan'),max_length=200, null=True, blank=True)
	slogan_color = ColorField(_('Slogan Color'), default='#0695ab')
	has_line = models.BooleanField(default=False)
	line_color = ColorField(_('Line Color'), default='#444')

	def __inicode__(self):
		return 'Hone Feature Container'

	def __str__(self):
		return 'Hone Feature Container'

class HomeFeatureItem(CMSPlugin):
	icon = models.ImageField(upload_to='features/icons/', blank=False, null=False)
	description = HTMLField()

	def __inicode__(self):
		return 'Hone Feature Item'

	def __str__(self):
		return 'Hone Feature Item'

class ParallaxContainer(CMSPlugin):
	padding_top = models.IntegerField(_('Padding top'), null=False, blank=False, default=20)
	background_img = models.ImageField(_('Background Image'), upload_to='parallax/bg/', null=False, blank=False)
	min_height = models.CharField(_('Minimun height'), null=False, blank=False, default='200px', max_length=100)
	title = models.CharField(_('Container Title'), max_length=100, null=False, blank=False)
	title_color = ColorField(_('Title Color'), null=False, blank=False, default='#FFF')
	slogan = models.CharField(_('Container Slogan'), max_length=100, null=False, blank=False)
	slogan_color = ColorField(_('Slogan Color'), null=False, blank=False, default='#0695ab')

	def __unicode__(self):
		return 'Parallax Container'

	def __str__(self):
		return 'Parallax Containner'

class HadronButtonGroup(CMSPlugin):
	height = models.IntegerField(_('Button Group height'), null=False, blank=False, default=50)
	width = models.CharField(_('Button Group max width (% or px'), max_length=100, null=False, blank=False, default='80%')
	button_alignment = models.CharField(_('Button Alignment'), max_length=100, null=True, blank=False, choices=ALIGMENTS, default=None)

	def __unicode__(self):
		return 'Hadron Button Group'

	def __str__(self):
		return 'Hadron Button Group'

class HadronButton(CMSPlugin):
	button_label = models.CharField(_('Button Label'), max_length=100, null=False, blank=False, default='Button')
	label_color = ColorField(_('Button Label Color'), null=False, blank=False, default='#ffffff')
	button_border_color = ColorField(_('Button Border Color'), null=False, blank=False, default='#0695ab')
	button_position = models.CharField(_('Button Position'), max_length=100, choices=TITLE_POSITIONS, default='left')
	button_url = models.CharField(_('Button url'), max_length=200, null=False, blank=False, default='#')

	def __unicode__(self):
		return 'Hadron Button'

	def __str__(self):
		return 'Hadron Button'

class HomeIndustryContainer(CMSPlugin):
	title = models.CharField(_('Container Title'), max_length=100, null=False, blank=False)
	title_color = ColorField(_('Title Color'), null=False, blank=False, default='#FFF')
	slogan = models.CharField(_('Container Slogan'), max_length=100, null=False, blank=False)
	slogan_color = ColorField(_('Slogan Color'), null=False, blank=False, default='#0695ab')

	def __unicode__(self):
		return 'Home Industry Container'

	def __str__(self):
		return 'Home Industry Container'

class ItemOverview(CMSPlugin):
	title = models.CharField(_('Item Title'), max_length=200, null=False, blank=False)
	title_color = ColorField(_('Item Title Color'), null=False, blank=False, default='#0695ab')
	overview = models.TextField(_('Item Overview Text'), null=False, blank=False)

	def __str__(self):
		return 'Item Overview'
