import os
from django.core.exceptions import ValidationError
VIDEO_EXTENSION = ['.mp4', '.webm']


def validate_video_extension(value):
	if not value:
		return
	ext = os.path.splitext(value.name)[1]  # [0] returns path+filename
	if not ext.lower() in VIDEO_EXTENSION:
		raise ValidationError(u'Unsupported file extension for embeded video.\
        	 Only support [{}]'.format(','.join(VIDEO_EXTENSION)))