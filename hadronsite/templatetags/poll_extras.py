from django import template

register = template.Library()

@register.filter(name='cut')
def cut(value, args):
	"""Removes all values of arg from the given string"""
	value = value.replace(args, '')
	if value.startswith('...'):
		value = value[3:]
	return value
