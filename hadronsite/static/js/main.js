$(function() {
	// header search handler
	$(document).ready(function() {
		$('li.header-search, .header-search i').hover(function(elm) {
			console.log('Hover me');
			$('.header-search').addClass('active');
			$('.header-search input').focus();
		});
	});
	

	$(document).click(function(evt) {
		if (evt.target === $('.header-search').get(0) || $(evt.target).parent().get(0) === $('.header-search').get(0)){
			evt.stopPropagation();
		} else {
			$('.header-search').removeClass('active');
		}
	});

	$(window).scroll(function() {
		var top = $(window).scrollTop();
		if (top >= 30) {
			$('#header').addClass('sticky');
		} else {
			$('#header').removeClass('sticky');
		}
	});

}());