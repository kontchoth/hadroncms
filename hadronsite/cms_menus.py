from menus.base import NavigationNode
from menus.menu_pool import menu_pool
from cms.menu_bases import CMSAttachMenu
from django.utils.translation import ugettext_lazy as _

class TestMenu(CMSAttachMenu):

	name = _('Test Menu')

	def get_nodes(self, request):
		nodes = []
		n = NavigationNode(_('Homepage'), "/", 1)
		n2 = NavigationNode(_('Consulting'), "/consulting", 2)

		nodes.append(n)
		nodes.append(n2)
		return nodes

menu_pool.register_menu(TestMenu)