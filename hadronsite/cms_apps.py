
from cms.app_base import CMSApp
from cms.apphook_pool import apphook_pool
from django.utils.translation import ugettext_lazy as _

from .cms_menus import TestMenu

class TestMenuApp(CMSApp):
    name = _("TestMenu")
    urls = ["hadronsite.urls"]
    menus = [TestMenu]

apphook_pool.register(TestMenuApp)